## Home work NoSql

1. Find all books authored by "Author 5" that have been borrowed by any user but not yet returned.
2. List all books published before the year 1980 and have more than 5 copies available.
3. Find the top 5 most recently published books in the "Fantasy" genre.
4. Count the number of books available for each genre.
5. Find all books that have never been borrowed by any user.

## PROJECT

1. dari project 2 dbt , sudah memasukkan data csv --> postgre?
2. data csv postgre --> source table
3. extract menggunakan python / pyspark --> ambil data dari postgre (csv) --> masukkan ke hdfs + hive / BigQuery / Snowflake
4. di project 2 kalian punya tugas untuk mengerjakan query data mart? kalian juga punya datawarehouse (kalau menurut kalian ada datawarehouse nya)
5. dari point no 3 dan 4 orchestrate dengan menggunakan airflow

1. code DAG
2. rekam saat kalian running DAG tersebut
3. ss hasilnya yang berada di hdfs + hive / BigQuery / Snowflake